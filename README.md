# pvault #

A simple password storage program using AES-128 encryption to store the passwords, written in C.
Both for Linux and Windows

### Description ###

* Simple tool
* Ver. 0.1 beta

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
	* [tiny-AES12-C](https://github.com/kokke/tiny-AES128-C)
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
