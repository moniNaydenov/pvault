CC=gcc
DEFS=-D_XOPEN_SOURCE=500 -D_BSD_SOURCE
CFLAGS=-Wall -g -std=c99 -pedantic $(DEFS)

OBJECTFILES=aes.o worker.o main.o crypt.o worker.o

default: pvault

all: pvault

pvault: $(OBJECTFILES)
		$(CC) $(LDFLAGS) -o $@ $^

%.o: %.c
		$(CC) $(CFLAGS) -c -o $@ $<
clean:
		rm -f $(OBJECTFILES) pvault
