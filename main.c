#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "crypt.h"
#include "worker.h"

void free_resources();

int main(int argc, char** argv) {
    unsigned char key[17] = "1234567890123456";
    unsigned char input[1024] = "this is just a test stringaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbdbbbaa";
    unsigned char output[1024];
    memset(output, 0, 1024);
    printf("\n%s\n\n", input);
    hex_encrypt(input, key, output);
    printf("%s\n\n", output);
    memset(input, 0, 1024);
    memcpy(input, output, 1024);    
    memset(output, 0, 1024);
    hex_decrypt(input, key, output);
    printf("%s\n\n", output);
    init();
    free_resources();
    return 0;
}

void free_resources() {
}