#ifndef WORKER_H
#define WORKER_H

#include <time.h>


void init();
void open_storage_file();
void read_user_key();
int list_stored_passwords();
void add_password();
void get_password();
void set_password(int i, unsigned char* name, unsigned char* password, unsigned char* key);
void bail_out(char* message);
void clear();
void handle_user();

void flush_stdin();


#endif