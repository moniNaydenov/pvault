#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "crypt.h"
#include "worker.h"

static unsigned char userkey[BUFFSIZE];
static time_t lastcheck; 
static FILE * storage;

void init() {
    memset(userkey, 0, BUFFSIZE);
    open_storage_file();
    //TODO: check beginning of file for consistency 
    read_user_key();
    while (1) {
        handle_user();
    }
}

void open_storage_file() {
    storage = fopen("./storage", "a+");
    if (storage == NULL) {
        bail_out("opening storage file");
    }
}

void read_user_key() {
    clear();
    (void) printf("Please enter your master key (16 chars max): ");
    (void) scanf("%16s", userkey);
    flush_stdin();
}

void bail_out(char* message) {
    (void) fprintf(stderr, "ERROR: \n\t%s: %s", message, strerror(errno));
    exit(EXIT_FAILURE);
}

int list_stored_passwords() {
    char line[BUFFSIZE];
    char name[BUFFSIZE];
    int l = 0;
    if (fseek(storage, 0, SEEK_SET) != 0) {
        bail_out("fseek");
    }
    clear();
    printf("====\nCurrently stored passwords:\n====\n");


    while (fgets(&line[0], BUFFSIZE, storage) != NULL) {
        sscanf(&line[0], "%[^:]", &name[0]);
        printf("\t%d. %s\n", ++l, name);
    }
    return l;
}

void add_password() {
    char compiled_line[BUFFSIZE];    
    unsigned char line[BUFFSIZE];
    unsigned char encryptedpass[BUFFSIZE];

    memset(compiled_line, 0, BUFFSIZE);

    clear();
    printf("====\nADD A PASSWORD\n====\n\n\n");
    do {
        printf("Enter identifier/name (one word: a-z, A-Z, 0-9): ");
        memset(line, 0, BUFFSIZE);
        (void) scanf("%512s", (char *)line);
        flush_stdin();
    } while (sscanf((char *) line, "%[a-zA-Z0-9]", compiled_line) != 1);
    printf("Identifier chosen: %s\n", compiled_line);
    strncat(compiled_line, ":", 1);
    printf("Enter password (all characters except empty spaces and new lines): ");
    memset(line, 0, BUFFSIZE);
    (void) scanf("%512s", line);
    flush_stdin();
    printf("Password chosen: %s\n", line);
    hex_encrypt(line, userkey, encryptedpass);
    strncat(compiled_line, (char *)encryptedpass, BUFFSIZE/2);
    strncat(compiled_line, "\n", 1);    
    printf("Saving to storage file...");
    if (fseek(storage, 0, SEEK_END) != 0) {
        bail_out("add_password fseek");
    }
    (void) fwrite(compiled_line, 1, strlen(compiled_line), storage);
    fflush(storage);
    (void) printf("done.\n");

}

void get_password() {    
    int count = list_stored_passwords();
    int choice = 0;
    char buff[WORDSIZE];
    unsigned char line[BUFFSIZE];
    unsigned char encryptedpass[BUFFSIZE];
    unsigned char pass[BUFFSIZE];
    do {
        memset(buff, 0, WORDSIZE);
        printf("Which password would you like to use? (1-%d): ", count);
        scanf("%3s", buff);
        flush_stdin();
        choice = atoi(buff);
    } while (choice <= 0 || choice > count);

    if (fseek(storage, 0, SEEK_SET) != 0) {
        bail_out("fseek");
    }
    for (int i = 0; i < choice; i++) {
        (void *)fgets((char *)&line[0], BUFFSIZE, storage);
    }
    memset(encryptedpass, 0, BUFFSIZE);
    memset(pass, 0, BUFFSIZE);
    sscanf((char *)&line[0], "%*[^:]:%s", &encryptedpass[0]);
    //printf("decrypting: %s\n", encryptedpass);
    hex_decrypt(encryptedpass, userkey, pass);

    printf("PASSWORD: %s\n", pass);

    /*
        #include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>
...
Display *display;
unsigned int keycode;
display = XOpenDisplay(NULL);
...
keycode = XKeysymToKeycode(display, XK_Pause);
XTestFakeKeyEvent(display, keycode, True, 0);
XTestFakeKeyEvent(display, keycode, False, 0);
XFlush(display);

You will need to link with the -lX11 -lXtst
    */
}

void handle_user() {
    int choice = 0;
    char line[WORDSIZE];
    clear();
    printf("What would you like to do?\n");
    printf("\t1. Get a password\n");
    printf("\t2. Read a password\n");
    printf("\t3. Add a password\n");
    printf("\t4. Update a password\n");
    printf("\t5. Delete a password\n");
    printf("\t10. Change your master key\n");
    printf("\n\n\n");
    do {
        memset(line, 0, WORDSIZE);
        printf("Your choice (1-10): ");    
        scanf("%2s", line);
        flush_stdin();
        choice = atoi(line);

    } while (choice <= 0);
    switch (choice) {
        case 1: //get password
            get_password();
            break;
        case 2: //view password
            break;
        case 3: //add pass
            add_password();
            break;
        case 4: //update pass
            break;
        case 5: //delete pass
            break;
        case 10: //change master pass
            break;
    }
    printf("Press enter to continue...");
    getchar();
    flush_stdin();
}

void clear() {
    (void) system("clear");
}

void flush_stdin() {
    (void) scanf("%*[^\n]");
}