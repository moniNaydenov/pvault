#ifndef CRYPT_H
#define CRYPT_H

#define BUFFSIZE 1024
#define WORDSIZE 16

void hex_encrypt(unsigned char* input, unsigned char* key, unsigned char* output);
void hex_decrypt(unsigned char* input, unsigned char* key, unsigned char* output);

void fix_key(unsigned char* input_key, uint8_t* output_key);

void tohex(unsigned char* input, unsigned char* output, int size);

void fromhex(unsigned char* input, unsigned char* output, int size);

#endif