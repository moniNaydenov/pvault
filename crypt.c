#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "aes.h"
#include "crypt.h"


void hex_encrypt(unsigned char* input, unsigned char* key, unsigned char* output) {
    size_t s = strlen((char *)input);
    int left = s % WORDSIZE;

    
    uint8_t temp_input[WORDSIZE];
    uint8_t temp_output[WORDSIZE];
    uint8_t fixed_key[WORDSIZE];
    uint8_t byte_output[BUFFSIZE];
    fix_key(key, fixed_key);
    memset(output, 0, BUFFSIZE); //clears out output
    memset(byte_output, 0, BUFFSIZE); //clears out output
    int i = 0;
    for (i = 0; i < s; i += WORDSIZE) {
        memset(temp_input, 0, WORDSIZE);
        memcpy(temp_input, input + i, WORDSIZE);
        AES128_ECB_encrypt(temp_input, fixed_key, temp_output);
        memcpy(byte_output + i, temp_output, WORDSIZE);
    }
    if (left > 0) {
        i -= WORDSIZE;
        memset(temp_input, 0, WORDSIZE);
        memcpy(temp_input, input + i, left);
        AES128_ECB_encrypt(temp_input, fixed_key, temp_output);
        memcpy(byte_output + i, temp_output, WORDSIZE);
        i += WORDSIZE;
    }
    printf("length: %d\n", i);
    tohex(byte_output, output, i);
    printf("encrypted: %s\n", output);
}

void hex_decrypt(unsigned char* input, unsigned char* key, unsigned char* output) {
    size_t s = strlen((char *)input) / 2;
    uint8_t temp_input[WORDSIZE];
    uint8_t temp_output[WORDSIZE];
    uint8_t fixed_key[WORDSIZE];
    uint8_t byte_input[BUFFSIZE];

    memset(byte_input, 0, BUFFSIZE);
    memset(output, 0, BUFFSIZE); //clears out output

    fix_key(key, fixed_key);    

    fromhex(input, byte_input, s);

    for (int i = 0; i < s; i += WORDSIZE) {
        memset(temp_input, 0, WORDSIZE);
        memcpy(temp_input, byte_input + i, WORDSIZE);

        AES128_ECB_decrypt(temp_input, fixed_key, temp_output);
        memcpy(output + i, temp_output, WORDSIZE);
    }    
}

void fix_key(unsigned char* input_key, uint8_t* output_key) {
    size_t s = strlen((char *)input_key);
    s = s <= WORDSIZE ? s : WORDSIZE;
    memset(output_key, '0', WORDSIZE);
    memcpy(output_key, input_key, s);
}

void tohex(unsigned char* input, unsigned char* output, int size) {
    for (int i = 0; i < size; i++) {
        sprintf((char *)&output[i*2], "%.2x", input[i]);

        printf("%.1s - %.2s\n", &input[i], &output[i*2]);
    }
}

void fromhex(unsigned char* input, unsigned char* output, int size) {
    for (int i = 0; i < size*2; i+=2) {
        sscanf((char *)&input[i], "%2x", (unsigned int *)&output[i/2]);
    }
}